import ansible_runner
import os
import shutil

# Create global path variables
workdir = os.path.dirname(os.path.abspath(__file__))
ansibledir = os.path.join(workdir,'artifacts')
envdir = os.path.join(workdir,'env')
envfile = os.path.join(envdir,'extravars')

# Define main function
def gather_configs():

    # Call setup function
    setup()

    # Initiate ansible playbook
    r = ansible_runner.run(
            private_data_dir=workdir, 
            playbook='gather_cisco_config.yaml'
        )
    # # Call cleanup function 
    # cleanup()

# Define setup as a function to create an environment variable file to pass Gitlab CI env vars via python to ansible.
def setup():
    os.makedirs((envdir), exist_ok=True)
    with open(envfile, "w") as f:
        f.write("---\n")
        f.write(f"CISCO_PASS: {os.getenv('CISCO_PASS')}")
        
# Define cleanup function
def cleanup():
    if os.path.exists(envdir): shutil.rmtree(envdir)	
    if os.path.exists(ansibledir): shutil.rmtree(ansibledir)	

# Execute function when file is run as script, not imported.
if __name__=="__main__":
    gather_configs()