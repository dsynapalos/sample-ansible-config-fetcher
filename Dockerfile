# Source from ubuntu:22.04 public image https://hub.docker.com/_/ubuntu
FROM ubuntu:22.04
# Copy local app to working directory inside container
COPY ./project /root/project
# Set working directory (permanent cd before command execution)
WORKDIR /root/project
# Run install commands
RUN apt update
RUN apt upgrade -y
RUN apt install python3 python3-pip -y
RUN pip3 install -r requirements.txt

